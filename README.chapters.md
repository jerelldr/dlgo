# Chapter 3

- The two players of a game of Go are best encoded by using an enum.
- A point on the Go board is characterized by its immediate neighborhood.
- A move in Go is either playing, passing, or resigning.
- Strings of stones are connected groups of stones of the same color. Strings are important to efficiently check for captured stones after placing a stone.
- The Go Board has all the logic of placing and capturing stones.
- In contrast, GameState keeps track of whose turn it is, the stones currently on the board, and the history of previous states.
  Ko can be implemented by using the situational superko rule.
- Zobrist hashing is an important technique to efficiently encode game-play history and speed up checking for ko.
- A Go-playing agent can be defined with one method: select_move.
- Your random bot can play against itself, other bots, and human opponents.

# Chapter 4

- Finding the best move with the minimax algorithm
- Pruning minimax tree search to speed it up
- Applying Monte Carlo tree search to games
